// components
import Head from 'next/head'
import SignUp from '../components/PageComponents/SignUp'

export default function Page() {
  return (
    <>
      <Head>
        <title>Sign up</title>
        <link rel='preconnect' href='https://fonts.googleapis.com' />
        <link rel='preconnect' href='https://fonts.gstatic.com' crossOrigin />
        <link href='https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap' rel='stylesheet' />
      </Head>
      <SignUp />
    </>
  )
}
