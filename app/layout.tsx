import * as React from 'react'

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang='en'>
      <body style={{ background: 'rgba(244, 249, 255, 1)', backgroundImage: "url('/static/background.svg')" }}>
        {children}
      </body>
    </html>
  )
}
