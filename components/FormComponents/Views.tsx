// libs
import styled, { css } from 'styled-components'
import { FieldInputProps } from 'formik/dist/types'
import { FormikProps } from 'formik'
import { FieldProps } from 'formik/dist/Field'

export const SimpleInput = styled.input<any>`
  width: 100%;
  height: 48px;
  padding: 10px 10px 10px 20px;
  gap: 10px;
  border-radius: 10px;
  font-size: 16px;
  font-family: Inter;
  font-weight: 400;
  color: #151d51;
  background-color: #ffffff;
  line-height: 19.36px;
  text-align: left;
  box-sizing: border-box;
  border: 2px solid #151d5133;
`

export const Input = styled(({ isError, isDisabled, isCorrect, ...rest }) => <SimpleInput {...rest} />)<{
  isError: boolean
  isDisabled: boolean
  isCorrect: boolean
}>`
  :focus {
    border: 2px solid #151d51;
  }

  :active,
  :focus {
    color: #151d51;
  }

  ${({ isError }) =>
    isError &&
    css`
      border-color: rgba(237, 95, 89, 1);
      background-color: rgba(237, 95, 89, 0.1);
    `}

  ${({ isDisabled }) =>
    isDisabled &&
    css`
      border-color: rgba(21, 29, 81, 0.2);
      background-color: rgba(21, 29, 81, 0.1);
    `}

    ${({ isCorrect }) =>
    isCorrect &&
    css`
      border-color: #27b274;
      color: #27b274;
      background-color: #ffffff;
      -webkit-box-shadow: inset 0 0 0 50px #5cd6c01a;
      -webkit-text-fill-color: #000;
    `}
`

export const InputPassword = styled(({ isError, isDisabled, ...rest }) => <SimpleInput {...rest} />)<any>`
  padding-right: 45px;

  ${({ isError }) =>
    isError &&
    css`
      border-color: rgba(237, 95, 89, 1);
      background-color: rgba(237, 95, 89, 0.1);
    `}

  ${({ isDisabled }) =>
    isDisabled &&
    css`
      border-color: rgba(21, 29, 81, 0.2);
      background-color: rgba(21, 29, 81, 0.1);
    `}

    :focus {
    border-color: rgba(21, 29, 81, 1);
  }
`

export const InfoMessage = styled.span`
  color: rgba(237, 95, 89, 1);
  position: absolute;
  font-family: Inter;
  font-size: 13px;
  font-weight: 400;
  line-height: 18px;
  bottom: 0px;
  left: 20px;
`

export const FieldWrapper = styled.div`
  position: relative;
  width: 315px;

  img {
    position: absolute;
    right: 20px;
    top: 12px;
  }
`

export const InfoMessagePassword = styled.p<{ correct: boolean; activeCheckPassword: boolean }>`
  margin-left: 20px;
  color: #4a4e71;
  font-family: Inter;
  font-size: 13px;
  font-weight: 400;
  line-height: 18px;
  text-align: left;
  margin-block-start: 4px;
  margin-block-end: 4px;

  ${({ correct, activeCheckPassword }) =>
    activeCheckPassword &&
    (correct
      ? css`
          color: #009796;
        `
      : css`
          color: #ed5f59;
        `)}
`
