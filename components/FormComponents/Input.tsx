import * as React from 'react'
// themes
import { Input, InfoMessage, FieldWrapper } from './Views'
// types
import { FieldProps } from 'formik/dist/Field'
import { FormikState } from 'formik/dist/types'

interface IInputFieldProps extends FieldProps {
  label: string
  disabled: boolean
}

const InputField: React.FC<IInputFieldProps & FormikState<{ email: string; password: string }>> = ({
  field,
  form,
  ...props
}) => {
  const { label, ...rest } = props
  const isTouched = form.touched[field.name]
  const isError = props.errors.email && props.touched.email

  return (
    <FieldWrapper>
      {label && <label htmlFor={label}>{label}</label>}
      <Input {...field} {...rest} isError={isError} isDisabled={rest.disabled} isCorrect={isTouched && !isError} />
      {isError && <InfoMessage>{props.errors.email}</InfoMessage>}
    </FieldWrapper>
  )
}

export default InputField
