import * as React from 'react'
// themes
import { FieldWrapper, InfoMessagePassword, InputPassword } from './Views'
// types
import { FieldProps } from 'formik/dist/Field'
import { FormikState } from 'formik/dist/types'

const PasswordField: React.FC<FieldProps & FormikState<{ email: string; password: string }>> = ({
  field,
  form,
  ...props
}) => {
  const [showHidePassword, changeShowHidePassword] = React.useState(false)
  const [activeCheckPassword, changeActiveCheckPassword] = React.useState(false)
  const itHasLowAndUpLetter = /[A-Z]/.test(field.value) && /[a-z]/.test(field.value)
  const itDig = /\d/.test(field.value)
  const lenght = field.value.replaceAll(' ', '').length >= 8

  const handleFocusEvent = (e: React.FocusEvent<HTMLInputElement>) => {
    changeActiveCheckPassword(true)
  }

  return (
    <FieldWrapper>
      <img
        src={showHidePassword ? '/static/showPassword.svg' : '/static/hidePassword.svg'}
        width={24}
        height={24}
        onClick={() => changeShowHidePassword(!showHidePassword)}
      />
      <InputPassword
        type={showHidePassword ? 'text' : 'password'}
        {...field}
        placeholder='Create your password'
        onFocus={handleFocusEvent}
      />
      <InfoMessagePassword activeCheckPassword={props.touched.password && activeCheckPassword} correct={lenght}>
        Has at least 8 characters (no spaces)
      </InfoMessagePassword>
      <InfoMessagePassword
        activeCheckPassword={props.touched.password && activeCheckPassword}
        correct={itHasLowAndUpLetter}
      >
        Uppercase and lowercase letters
      </InfoMessagePassword>
      <InfoMessagePassword activeCheckPassword={props.touched.password && activeCheckPassword} correct={itDig}>
        1 digit minimum
      </InfoMessagePassword>
    </FieldWrapper>
  )
}

export default PasswordField
