'use client'
import * as React from 'react'
// libs
import { Formik, FormikProps, Field } from 'formik'
// components
import Input from '../../FormComponents/Input'
import PasswordField from '../../FormComponents/Password'
// themes
import { SignButton, Title, WrapperForm, WrapperMain } from './Views'
// utils
import { isEmptyField, validateEmail } from '../../../utils/validations'

interface ISignUpFormValues {
  email: string
  password: string
}

const SignUpForm = () => (
  <WrapperMain>
    <Title>Sign up</Title>
    <Formik
      initialValues={{ email: '', password: '' }}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2))
          actions.setSubmitting(false)
        }, 1000)
      }}
    >
      {(props: FormikProps<ISignUpFormValues>) => (
        <WrapperForm>
          <Field
            name='email'
            placeholder='Email'
            validate={validateEmail}
            component={Input}
            {...props}
            // disabled={true}
          />
          <Field
            name='password'
            placeholder='Create your password'
            validate={isEmptyField}
            component={PasswordField}
            {...props}
          />
          <SignButton type='submit'>Sign up</SignButton>
        </WrapperForm>
      )}
    </Formik>
  </WrapperMain>
)

export default SignUpForm
