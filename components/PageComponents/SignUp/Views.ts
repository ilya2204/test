// libs
import styled from 'styled-components'
import { Form } from 'formik'

export const SignButton = styled.button`
  background: linear-gradient(110.46deg, #70c3ff 12.27%, #4b65ff 93.92%);
  width: 240px;
  height: 48px;
  margin-top: 40px;
  padding: 15px 32px 15px 32px;
  gap: 10px;
  border-radius: 30px;
  border-width: 0px;
  cursor: pointer;
  font-family: Inter;
  font-size: 16px;
  font-weight: 700;
  line-height: 14px;
  text-align: center;
  color: #ffffff;
`

export const WrapperForm = styled(Form)`
  display: flex;
  flex-direction: column;
  align-items: center;

  input {
    margin-bottom: 20px;
  }
`

export const WrapperMain = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`

export const Title = styled.p`
  font-family: Inter;
  font-size: 28px;
  font-weight: 700;
  line-height: 28px;
  text-align: center;
  margin-bottom: 38px;
  color: #4a4e71;
`
